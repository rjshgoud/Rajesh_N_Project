package com.util;

import java.util.HashMap;
import java.util.Map;

import com.bean.UserHealthInfoBean;

public class CommonUtil {
	static int count=0;
	static int goodHabitCount=0;
	/**
	 * To prepare the given input 
	 * @return
	 */
	public static UserHealthInfoBean getInput() {
		Map<String,String> currentHealthMap=new HashMap<String,String>();
		Map<String,String> habitsMap=new HashMap<String,String>();
 		UserHealthInfoBean user=new UserHealthInfoBean();
		user.setName("Norman Gomes");
		user.setGender("Male");
		user.setAge(34);
		
		currentHealthMap.put("Hypertension", "No");
		currentHealthMap.put("Blood pressure", "No");
		currentHealthMap.put("Blood sugar", "No");
		currentHealthMap.put("Overweight", "Yes");
		
		habitsMap.put("Smoking", "No");
		habitsMap.put("Alcohol", "Yes");
		habitsMap.put("Daily exercise", "Yes");
		habitsMap.put("Drugs", "No");
		
		user.setCurrentHealth(currentHealthMap);
		user.setHabits(habitsMap);
		return user;		
	}
	/**
	 * To calculate Gender Rule Amount
	 * @param gender
	 * @return
	 */
	public static Integer getGenderRuleAmount(String gender) {
		int amount=5000;
		int temp=0;
		if(gender.equalsIgnoreCase("Male")) {
			temp=amount*2/100;
			System.out.println("gender extra amount:"+temp);
			return temp;
		}
		return 0;
		
	}
	/**
	 * To calculate current health amount
	 * @param currentHealthMap
	 * @return
	 */
	public static Integer getCurrentHealthAmount(Map<String,String> currentHealthMap) {
		 count=0;
		 int amount=5000;
		currentHealthMap.forEach((k,v)->{
			System.out.println("Item : " + k + " Count : " + v);
			if("Yes".equals(v)){
				System.out.println(v);
				count++;
			}
		});
		System.out.println(count);
		int temp=amount*count/100;
		System.out.println("current health extra amount:"+temp);
		return temp;
	}
	/**
	 * To calculate habits amount
	 * @param habitsMap
	 * @return
	 */
	public static Integer getHabitsAmount(Map<String,String> habitsMap) {
		count=0;
		goodHabitCount=0;
		int amount=5000;
		habitsMap.forEach((k,v)->{
			System.out.println("Item : " + k + " Count : " + v);
			if("Daily exercise".equals(k)&&"Yes".equals(v)) {
				//System.out.println(k+","+v);
				goodHabitCount=goodHabitCount+1;
			}
			else if("Yes".equals(v)){
				//System.out.println(k+","+v);
				count=count+1;
			}
		});
		goodHabitCount=goodHabitCount*3;
		count=count*3;
		System.out.println("bad habit count:"+count);
		System.out.println("good habit count:"+ goodHabitCount);
		int increaseAmount=amount*count/100;
		int decreaseAmount=amount*goodHabitCount/100;
		System.out.println("habits extra amount:"+(increaseAmount-decreaseAmount));
		return (increaseAmount-decreaseAmount);
	}
}
