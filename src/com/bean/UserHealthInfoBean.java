package com.bean;

import java.util.Map;

public class UserHealthInfoBean {
	
	private String name;
	private String gender;
	private Integer age;
	private Map<String,String> currentHealth;
	private Map<String,String> habits;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Map<String, String> getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(Map<String, String> currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Map<String, String> getHabits() {
		return habits;
	}
	public void setHabits(Map<String, String> habits) {
		this.habits = habits;
	}
	
	

}
