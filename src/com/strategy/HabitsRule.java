package com.strategy;

import com.bean.UserHealthInfoBean;
import com.inter.PremiumInterface;
import com.inter.PremiumStrategy;
/**
 * 
 * @author Rajesh N
 *
 */
public class HabitsRule implements PremiumStrategy{

	static int goodHabitCount=0;
	static int count=0;
	@Override
	/**
	 * TO get Habits rules amount
	 */
	public int doOperations(UserHealthInfoBean userHealthInfoBean) {
		count=0;
		goodHabitCount=0;
		int amount=5000;
		userHealthInfoBean.getHabits().forEach((k,v)->{
			System.out.println("Item : " + k + " Count : " + v);
			if("Daily exercise".equals(k)&&"Yes".equals(v)) {
				//System.out.println(k+","+v);
				goodHabitCount=goodHabitCount+1;
			}
			else if("Yes".equals(v)){
				//System.out.println(k+","+v);
				count=count+1;
			}
		});
		goodHabitCount=goodHabitCount*3;
		count=count*3;
		System.out.println("bad habit count:"+count);
		System.out.println("good habit count:"+ goodHabitCount);
		int increaseAmount=amount*count/100;
		int decreaseAmount=amount*goodHabitCount/100;
		System.out.println("habits extra amount:"+(increaseAmount-decreaseAmount));
		return (increaseAmount-decreaseAmount);
	}
}
