package com.strategy;

import com.bean.UserHealthInfoBean;
import com.inter.PremiumInterface;
import com.inter.PremiumStrategy;
/**
 * 
 * @author Rajesh N
 *
 */
public class CurrentHealthRule implements PremiumStrategy{

	public static int count=0;
	@Override
	/**
	 * To get current health amount
	 */
	public int doOperations(UserHealthInfoBean userHealthInfoBean) {
		count=0;
		 int amount=5000;
		userHealthInfoBean.getCurrentHealth().forEach((k,v)->{
			System.out.println("Item : " + k + " Count : " + v);
			if("Yes".equals(v)){
				System.out.println(v);
				count++;
			}
		});
		System.out.println(count);
		int temp=amount*count/100;
		System.out.println("current health extra amount:"+temp);
		return temp;
	}


}
