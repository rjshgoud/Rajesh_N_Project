package com.strategy;

import com.bean.UserHealthInfoBean;
import com.inter.PremiumInterface;
import com.inter.PremiumStrategy;
/**
 * 
 * @author Rajesh N
 *
 */
public class GenderRule implements PremiumStrategy{

	@Override
	/**
	 * To get gender rule amount
	 */
	public int doOperations(UserHealthInfoBean userHealthInfoBean) {
		int amount=5000;
		int temp=0;
		String gender=userHealthInfoBean.getGender();
		if(gender.equalsIgnoreCase("Male")) {
			temp=amount*2/100;
			System.out.println("gender extra amount:"+temp);
			return temp;
		}
		return 0;
	}


}
