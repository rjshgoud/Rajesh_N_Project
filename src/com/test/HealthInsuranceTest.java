package com.test;

import java.io.File;

import com.bean.UserHealthInfoBean;
import com.strategy.CurrentHealthRule;
import com.strategy.GenderRule;
import com.strategy.HabitsRule;
import com.util.CommonUtil;

/**
 * 
 * @author Rajesh N This is main class
 */
public class HealthInsuranceTest {

	public static void main(String[] args) {
		try {
			String first = "Health Insurance Premium for Mr. ";
			Integer amount = new Integer(5000);
			String output = "";

			UserHealthInfoBean userHealthInfoBean = new UserHealthInfoBean();
			// get the input value
			userHealthInfoBean = CommonUtil.getInput();

			String name = userHealthInfoBean.getName();
			String lastName = name.substring(name.lastIndexOf(' ') + 1).trim();

			File file = new File("/home/emidstest01/Desktop/Rajesh N/HealthInsurancePro/src/com/strategy");
			
	        File[] files = file.listFiles();
	        Context context1=null;
	        int finalAmount=0;
	        for(File f: files){
	        	System.out.println(f.getName());
	        	String s[]=f.getName().split("\\.");
	            context1 = new Context(Class.forName("com.strategy."+s[0]).newInstance());
	            finalAmount=finalAmount+context1.executeStrategy(userHealthInfoBean);
	        }
			
			
			// strategy implementation
			/*Context context = new Context(new GenderRule());
			int genderAmount = context.executeStrategy(userHealthInfoBean);
			context = new Context(new CurrentHealthRule());
			int currentHealthAmount = context.executeStrategy(userHealthInfoBean);
			context = new Context(new HabitsRule());
			int habitsAmount = context.executeStrategy(userHealthInfoBean);*/

			//Integer finalAmount = amount + genderAmount + currentHealthAmount + habitsAmount;
	        Integer finalAmount1 = finalAmount+amount;
			if (userHealthInfoBean.getAge() < 18) {
				output = first.concat((lastName + ":Rs.").concat(amount.toString()));
			} else if (userHealthInfoBean.getAge() >= 18 && userHealthInfoBean.getAge() <= 40) {
				Integer temp = new Integer(amount * 10 / 100);
				finalAmount = finalAmount + temp;
				output = first.concat((lastName + ":Rs.").concat(finalAmount1.toString()));
			} else {
				System.out.println("greater than 40");
				int difference = userHealthInfoBean.getAge() - 40;
				int percent = 20;
				int result = difference / 5;
				int finalPercent = result * percent;
				Integer temp = new Integer(amount * result / 100);
				output = first.concat((lastName + ":Rs.").concat(finalAmount1.toString()));
			}

			System.out.println(output);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * UserHealthInfoBean userHealthInfoBean=new UserHealthInfoBean();
		 * PremiumCalculationService premiumCalculationService = new
		 * PremiumCalculationService(); // get the input value
		 * userHealthInfoBean=CommonUtil.getInput();
		 * System.out.println(premiumCalculationService.calculate(userHealthInfoBean));
		 */
	}
}
