package com.test;

import com.bean.UserHealthInfoBean;
import com.inter.PremiumStrategy;

public class Context {
	
	private PremiumStrategy strategy;
	/*public Context(PremiumStrategy strategy){
	      this.strategy = strategy;
	   }*/
	
	public Context(Object object){
	      this.strategy = (PremiumStrategy) object;
	   }

	   public int executeStrategy(UserHealthInfoBean userHealthInfoBean){
	      return strategy.doOperations(userHealthInfoBean);
	   }

}
