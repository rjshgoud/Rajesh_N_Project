package com.service;

import static org.junit.Assert.*;

import org.junit.Test;

import com.util.CommonUtil;

public class CalculatePremiumTestCase {

	@Test
	public void test() {
		PremiumCalculationService testCase=new PremiumCalculationService();
		String actual=testCase.calculate(CommonUtil.getInput());
		
		assertEquals("Health Insurance Premium for Mr.Gomes:Rs.6856", actual);
	}

}
