package com.service;

import com.bean.UserHealthInfoBean;
import com.inter.PremiumInterface;
import com.util.CommonUtil;

/**
 * 
 * @author Rajesh N
 *
 */
public class PremiumCalculationService implements PremiumInterface{
	
	
	String first="Health Insurance Premium for Mr. ";
	String middle="";
	Integer amount=new Integer(5000);
	String output="";
	
	@Override
	/**
	 * calculation logic
	 */
	public String calculate(UserHealthInfoBean userHealthInfoBean) {
		String name=userHealthInfoBean.getName();
		String lastName=name.substring(name.lastIndexOf(' ') + 1).trim();
		
		if(userHealthInfoBean.getAge()<18) {
			output=first.concat((lastName+":Rs.").concat(amount.toString()));			
		}
		else if(userHealthInfoBean.getAge()>=18&&userHealthInfoBean.getAge()<=40) {
			Integer temp=new Integer(amount*10/100);
			amount=amount+temp+CommonUtil.getGenderRuleAmount(userHealthInfoBean.getGender())+
					CommonUtil.getCurrentHealthAmount(userHealthInfoBean.getCurrentHealth())
			+CommonUtil.getHabitsAmount(userHealthInfoBean.getHabits());
			
			output=first.concat((lastName+":Rs.").concat(amount.toString()));			
		}
		else {
			System.out.println("greater than 40");
			int difference=userHealthInfoBean.getAge()-40;
			int percent=20;
			int result=difference/5;
			int finalPercent=result*percent;
			Integer temp=new Integer(amount*result/100);
			System.out.println(difference+","+result+","+temp);
			amount=amount+temp+CommonUtil.getGenderRuleAmount(userHealthInfoBean.getGender())+
					CommonUtil.getCurrentHealthAmount(userHealthInfoBean.getCurrentHealth())
			+CommonUtil.getHabitsAmount(userHealthInfoBean.getHabits());
			output=first.concat((lastName+":Rs.").concat(amount.toString()));
		}
		return output;
	}
}
