package com.inter;

import com.bean.UserHealthInfoBean;

public interface PremiumStrategy {
	
	public int doOperations(UserHealthInfoBean userHealthInfoBean);

}
