package com.junit.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CurrentHealthRuleUnit.class, GenderRuleUnit.class, HabitsRuleUnit.class })
public class AllTests {

}
