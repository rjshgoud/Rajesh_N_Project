package com.junit.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.strategy.CurrentHealthRule;
import com.strategy.GenderRule;
import com.test.Context;
import com.util.CommonUtil;

public class CurrentHealthRuleUnit {

	@Test
	public void test() {
		Context context = new Context(new CurrentHealthRule());
		int genderAmount = context.executeStrategy(CommonUtil.getInput());
		
		assertEquals(50, genderAmount);
	}

}
