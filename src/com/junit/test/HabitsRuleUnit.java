package com.junit.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.strategy.GenderRule;
import com.test.Context;
import com.util.CommonUtil;

public class HabitsRuleUnit {

	@Test
	public void test() {
		Context context = new Context(new GenderRule());
		int genderAmount = context.executeStrategy(CommonUtil.getInput());
		
		assertEquals(100, genderAmount);
	}

}
